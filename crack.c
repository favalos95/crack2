#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hash_guess = md5(guess, strlen(guess));

    if(strcmp(hash_guess, hash) == 0 ) {
        return 1;
    } else {
        return 0;
    }
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    //get filesize
    struct stat info;
    stat(filename, &info);
    int file_length = info.st_size;
    
    //debug
    printf("> filename: %s\n", filename);
    printf("> filelength: %d\n", file_length); 
    
    //allocate memory for file  
    char *data = malloc(file_length+1);
    
    //read file
    FILE *f = fopen(filename, "r");
    if(!f) {
        perror("Can't open file");
        exit(1);
    }
    fread(data, 1, file_length, f);
    fclose(f);

    // NOTE: from lecture notes
    // add null char to end
    data[file_length] = '\0';

    //change newlines to nulls
    int lines = 0;
    for(int i = 0; i < file_length; i++) {
        if(data[i] == '\n') {
            data[i] = '\0';
            lines++;
        }
    }

    printf("> lines: %d\n\n", lines);

    //allocate memory for array of pointers
    //then loop thru data, store pointers in each stringi
    //
    //"char *" represents the user's system's size of a char,
    //because we don't know how big it is.
    char **strings = malloc( ((lines + 1) * sizeof(char *)) );
    int string_idx = 0;
    for(int i = 0; i < file_length; i += strlen(data + i) + 1) {
            strings[string_idx] = data + i;
            string_idx++;
    }
    //store terminating null
    strings[string_idx] = NULL;
    
   // free(strings);
   // free(data);
    
    return strings;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    //debug
    printf("> Attempting to read files:\n\t%s\n\t%s\n\n", argv[1], argv[2]);

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    printf("%s\n", "> Checking hashes...");
    for(int i = 0; hashes[i] != NULL; i++) {
        printf("%s\t%s\n", "> Hash:", hashes[i]);
        for(int j = 0; dict[j] != NULL; j++) {
            if(tryguess(hashes[i], dict[j]) == 1) {
                printf("> key:\t%s\n", hashes[i]);
                printf("> dict:\t%s\n", dict[j]);
                printf("> ================\n");
            }
        }
    }
}